In Regedit disable the updates:
-------------------------------
[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU]
"NoAutoUpdate"="dword:1"

Disable the Windows Update Assistant:
-------------------------------------
This update keeps a copy in C:\WINDOWS\UpdateAssistant from where it installs itself in C:\Windows10Upgrade Also two tasks are created in Task Scheduler: \Microsoft\Windows\UpdateOrchestrator\UpdateAssistant which activates on logon, system reboot, etc. , \Microsoft\Windows\UpdateOrchestrator\UpdateAssistantCalendarRun which activates every 3 days at 10:30 AM.

1. Uninstal regularly through the control panel "Upgrade Advisor to Windows 10"
2. Delete the folder C:\WINDOWS\UpdateAssistant
3. Delete tasks in Task Scheduler \Microsoft\Windows\UpdateOrchestrator\UpdateAssistant and
\Microsoft\Windows\UpdateOrchestrator\UpdateAssistantCalendarRun