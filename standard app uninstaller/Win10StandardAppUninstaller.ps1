﻿param([string]$answer)

Write-Host "Hello. This Script will uninstall some of the Windows built in Apps"
$answer = Read-Host "Proceed? Y/N"

If($answer -eq "Y")
{
    Write-Host ""
    Write-Host "Starting unistalling process"
    Write-Host "Confirm with y for each App you want to uninstall"

    $answer = Read-Host ">> 3DBuilder"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *3d* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Alarms and Clock"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *windowsalarms* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Get Started"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *getstarted* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Video and TV"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *zunevideo* | remove-appxpackage
    }

    $answer = Read-Host ">> Finance"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *Finance* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Photos"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *Photos* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Groove Music"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *zunemusic* | remove-appxpackage
    }

    $answer = Read-Host ">> Maps"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *Maps* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Calendar and Mail"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *communicationsapps* | remove-appxpackage
    }

    $answer = Read-Host ">> News"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *News* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Messaging and Skype"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *messaging* | remove-appxpackage
    }

    $answer = Read-Host ">> Music"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *Music* | Remove-AppxPackage
    }

    $answer = Read-Host ">> OneNote"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *OneNote* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Skype"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *Skype* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Calculator"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *windowscalculator* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Solitaire"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *solitairecollection* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Sport"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *Sport* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Soundrecorder"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *soundrecorder* | remove-appxpackage
    }

    $answer = Read-Host ">> Sway"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *sway* | remove-appxpackage
    }

    $answer = Read-Host ">> Phone"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *commsphone* | remove-appxpackage
    }

    $answer = Read-Host ">> Phone Companion"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *windowsphone* | remove-appxpackage
    }

    $answer = Read-Host ">> Video"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *Video* | Remove-AppxPackage
    }

    $answer = Read-Host ">> Weather"
    if($answer -eq "Y") 
    {
        Get-AppxPackage *Weather* | Remove-AppxPackage
    }
}

else
{
    Write-Host ""
    Write-Host "Process terminated"
}