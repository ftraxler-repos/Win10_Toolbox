# Repository
  
This repository contains a collection of scripts and registry fixes for Win10.  
It is a public collection of useful Win10 hacks. 
Because of that I can not be responsible for any bugs and/or regular updates.  
  
## Why this collection?
  
Every new Update, Windows 10 comes with more and more unwanted features. You cannot uninstall apps, delete some folders, are forced to use apps/layouts and so on.  
Because I really hate such annoying things, I started to collect all the hacks I used to reset Windows 10 to my default and beloved settings.  
Maybe you could also use some of those scripts :-)  
  
## Contents
  
Every hack will be in a new folder and will contain some description and/or tutorial file.  
This repository contains cmd/batch scripts, powershell scripts and registry scripts.  
  
# License
  
These scripts were made of code found all around the web or made from personal experience. Therefore there is no license. 
This is just a collection of useful blog posts and scripts. The site of origin will be provided wherever possible.  
Feel free to use and optimize whatever you want.  
Please consider contributing your hacks/ideas/posts!  
  
The scripts and tutorials are provided in the hope to be useful,
but WITHOUT ANY WARRANTY to work as expected or to even work on the target machine.  
  
**PLEASE NOTE THAT EVERY USE OF THE PROVIDED SCRIPTS MIGHT HAVE HEAVY IMPACTS ON THE OPERATING SYSTEM.  
USE THESE SCRIPTS ONLY IF YOU KNOW WHAT YOU ARE DOING!**